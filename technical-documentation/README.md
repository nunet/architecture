## General

We provide current specifications and living documentation for all publicly developed platform components as an integral part of the same codebase. Each component and each module of a component are described by appropriately placed README.md files. This technical documentation serves also as functional specification of code and is aimed at coordination of internal and community developer work.

The goal of technical documentation and specifications is to enable developers and integrators to quickly understand and contribute to further development of the codebase or integrate it into third party business models and infrastructure. 

Developers should follow technical documentation guidelines as explained in this document. Tn the absence of clarity or in case of doubt, developers should follow the rule to make documentation so that any other reasonably skilled developer could understand the architecture, code structure, data structures, message payloads and therefore start contributing with minimal explicit communication to original authors of the code. Explicit specification and documentation of functinality of the code is an inseparable part of Open Source Software Development process. Inline coments and documentation near the code is encouraged as per our CONTRIBUTION_GUIDELINES is encouraged and considered supplemental to explicit documentation, but does not complement it.

Proper documentation of the contributed code as per these guidelines is one of the acceptance criterion of pull requests coming from both core team members and community developers.

## Structure within codebase
 
```
| {component_repository}
├── README.md          	# architecture, description and specification of functionality
├── {module_name}      	# (if exists within architecture)
├──── README.md        	# description and specification of module functionality
├──── {submodule_name} 	# (if exists within architecture)
├────── README.md 		# description and specifications of sub-module functionality
```

Documenation structured as provided above is available via regular gitlab web interface on the codebase (mostly used by developers actively contributing to the separate modules and submodules) and public gitbook space [Public Technical Documentation](https://docs.nunet.io/technical-documentation/) (mostly used by community developers, collaborators, partners and anybody wishing to understand implementation details of NuNet platform and start contributing / integrating it).

## Structure of each README.md file

TBD, following the [example](https://gitlab.com/nunet/device-management-service/-/blob/develop/onboarding/README.md) of device-management-service/onboarding module (subject to change based on DMS refactoring work which is currently underway). The precise structure and contents of the each documentation file may change in the course of platform evolution, but the should contain proper granularity of technical descriptions and aspects of it as provided by the existing examples.

All proposals for error correction or additions to documentation should be submitted via pull requests via gitlab and assigned for the module owner for review. All core developers, community members and general public is encouraged to sumit proposals for documentation improvements via such pull requests. Alternatively, external contributors may file feature requests for improvements (in case direct contribution is not possible of appropriate).

### Badges

It is recommended that each technical readme contains at least the followign badges:

* Current release version / tag;
* Pipeline status (could be split into pipeline stages);
* Coverage status and link to coverage report;

### Specification of functionality in Gherkin

### Data structures and models

### Specification of functionality in sequence diagrams

## Describing proposed and released features/components