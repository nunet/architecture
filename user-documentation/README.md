## General

Additionally to [technical documentation](../technical-documentation/README.md), each component, module or submodule may contain README-USR.md file, which contain freely formated developer notes and guidelines directed for end users or integrators of the implemented functionality. This user docuentation is available as regular files via gitlab interface as well as exposed in public gitlab space [Public User Documentation](https://app.gitbook.com/o/HmQiiAfFnBUd24KadDsO/s/sOm0ineFkMZP1wdHvwts/).

User documentation as explained above and is an experimental feature aimed at streamlining compiling user documentation which may not be directly to development and code, but contain some technical knowledge. This may change or discontinued in the future.
