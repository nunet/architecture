# Architecture & backlog management

This repo was setup for the private alpha / prototype development and served as an early architectural explanation as well as epics description, as long as we did not have gitlab business subscription.

Now, when epics descriptions are transferred fully to Gitlab, this repo is cleaned, made public and used for the architectural, roadmap structuring, backlog prioritization and development flow related discussions.
