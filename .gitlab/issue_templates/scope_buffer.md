Scope buffer contains uncertain tasks and their rough time estimation considered necessary for delivering the work package. We add scope buffers times to each packages time (ETTC) calculation for the purpose of managing uncertainty of design and technical research. As work package design and implementation are moving along and concrete issues are added, scope buffer time will be gradually decreasing.

/label ~scope_buffer
/title "Scope Buffer for {...}"