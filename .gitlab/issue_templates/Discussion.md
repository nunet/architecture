<!---
Please read this!

Before creating a new discussion topic, make sure to search for keywords in the issues
filtered by the "discussion" label:

- https://gitlab.com/groups/nunet/-/issues/?label_name=type::discussion

and verify the topic you're about to create isn't a duplicate.
--->

### Topic Summary

<!-- Summarize the discussion topic you want to create concisely. -->

### Topic Description

<!-- Describe the topic or idea you want to discuss. Keep it as concise and focused as possible for easier engagement. -->

### Why is this discussion important?

<!-- Explain why this topic is important or relevant to the community. -->

### Relevant links, logs, and/or screenshots

<!-- Share any relevant resources, logs, or screenshots that might help facilitate the discussion. Use code blocks (```) where applicable. -->

### NuNet components impacted by this discussion

<!-- Inform NuNet's components if applicable. -->

/label ~"type::discussion"
