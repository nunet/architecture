Here we define what are stakeholders of NuNet ecosystem, map them to components and define how we identify each in the platform. It is then related to job orchestration ontology, identification and authentication system and ultimately platform security model.

Docments where these were (to some extent) discussed are:
1. **main**: Mapping NuNet Stakeholders and Components [Dec 21, 2023](https://docs.google.com/presentation/d/11in0JpPXa05AFj_pDohN5WodPYijTDn9SsCbaJzHcH8/edit#slide=id.g1fcbec5b67a_0_184);
2. List of entities prepared for security policy control [Oct 7, 2023](https://docs.google.com/document/d/1M2HlBm_XF0EBLMXZJSgplmChD7xcT639i92H9XIa_6s/edit#bookmark=id.sbejwpjqacgl);

This folder contains following descriptions in separate comma separated files. These definitions are intended to be updated and changed as needed and as required by platform specifications:
1. [stakeholder categories](stakeholder_categories.md)
1. [stakeholders](stakeholders.md)
1. [component classes](component_classes.md)
1. [components](components.md)