# Stakeholders

Stakeholders are institutions or individuals who are using the platform and form the ecosystem. They can belong to different stakeholder categories explained [elsewhere](stakeholder_categories.md). 

| Stakeholder | Category | Description |
|--|--|--|
| Hardware Provider | Resource Providers | These own machines with certain amount of CPU, Memory, Disk space and GPU; also called "Compute provider" in most of our discussions |
| Data Providers | Resource Providers | These own data that is useful or needed for certain computational requirements. E.g. specific databases or sensor data that needs to be analyzed. We would like to integrate into data marketplaces (e.g. [Ocean](https://market.oceanprotocol.com/)) at some time to be able to leverage tokenized data in the near future|
| Algorithm Providers | Resource Providers | These own algorithms or models capable of performing certain tasks. Ex text to image converter. |
| Service Providers | Service Providers | Businesses, organizations and individuals, that want to use NuNet for their workflow requirements and to serve their end users |
| End Users | End Users | Individuals (or possibly businesses) served by Service Providers. NuNet basically does not want to have direct relations with these stakeholders or the whole stakeholder category. However, we are considering them because the need to build features into the platform that will eventually enable Service Providers to serve the End Users' needs, as well as for understanding the overall dynamics of the ecosystem |
| Direct Users | Direct Users | Individuals (or possibly businesses) that use NuNet platform for their needs and connect directly (without mediation of service providers) -- e.g. DeSci community researchers |
| Solution Enablers | Facilitators | Focused on serving businesses, individuals and institutions which a) do not want to engage in crypto payments b) need support or consulting for using the platform |
| Matchmaker Agents | Facilitators | Different mechanisms and algorithms developed by community developers or other business entities that match the consumers to the providers in the platform. |
| Reputation Providers | Trust Enablers | Generate and maintain a reputation score for participants
| Benchmarking Agents | Trust Enablers | Benchmark performance of provider assets (and submit that into the reputation database)|
| Job Validators | Trust Enablers | Assess whether the work was performed as intended |
| Cardano | Tokenomics Enablers | Provides a settlement layer implementation for Nunet tokenomics |
| Token holders | Tokenomics Enablers | People who hold NTX tokens and participate in NTX tokenomics |
| DEXs | Tokenomics Enablers | Decentralized Exchanges where NTX tokens can be exchanged to other crypto tokens |
| Traders | Tokenomics Enablers | Free participants in the crypto and fin markets which use NTX tokens |
