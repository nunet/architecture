# Stakeholder categories

We categorize participants in the envisioned NuNet platform and framework into categories based on which we can think about different behaviors tht will appear in the framework and functionalities that are needed to support them. Although we do not implement features that are needed for all categories or stakeholders, this is important for outlining needs for the platform architecture that will support all these categories and concrete [stakeholders](stakeholders.md) within them in the future. 

| Category | Description |
|--|--|
| Resource Providers | The supply side of the platforms which dedicate their resources to the platform |
| Service Providers | These use NuNet platform functionality to serve their customers |
| End Users | These use the dashboards built by Service Provider for their needs |
| Direct Users | These use the DMS API in their requirements |
| Facilitators | Provides different services to the different platform stakeholders and thus facilitate efficient operation of the whole ecosystem. e.g. connect the demand to the supply in the platform |
| Trust Enablers | Their services bring trust between platform participants |
| Partners | External functionalities or capabilities that we leverage |
| Tokenomics Enablers | Smart Contracts and Blockchain integration for settlements and payments layer |
