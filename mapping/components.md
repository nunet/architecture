# Components

These are actually implemented Platform components which will be offered to stakeholders to do the work on using the platform. While most of them simply have a repo on the [NuNet Gitlab](https://gitlab.com/nunet), the importance to categorize and describe them is different positioning of components with respect to [platform architecture](https://docs.nunet.io/nunet-licensing-policy/platform-component-classes), [licensing structure](https://docs.nunet.io/nunet-licensing-policy/licensing-regimes) and identification system.

| Component | Component Class | Description | Link |
|--|--|--|--|
| device-management-service | core platform component | By installing the package any computer can become a NuNet node; this package is needed if one wants to join NuNet and is the main component of the platform. | https://gitlab.com/nunet/device-management-service |
| management-dashboard | non-core platform component | Web Interface that enables compute providers to monitor and manage their machines (as well as jobs / allocations) effectively. The web interface connects to device-management-service via provided REST-API on that side. | https://gitlab.com/nunet/management-dashboard |
| Service Provider dashboard | non-core platform component |  Web Interface that is built by Service Providers for their end-users to access NuNet functionality. NuNet provides a REST-API on the device-management-service side for the Service Providers to be able to build dashboards customized for their target End Users. For selected use-cases, NuNet integrations team will be building these customized dashboards. | example dashboard for GPU on ML use-case https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-webapp |
| control-server | NuNet infrastructure | reCAPTCHA verification for the dashboard Interface -- part of *Service Provider Dashboard* | https://gitlab.com/nunet/control-server |
| oracle | non-core platform component | Validation layer within the platform for funding and reward transactions | https://gitlab.com/nunet/oracle |
| wallet-server | NuNet infrastructure | Generates a signature that lets the smart contract know what operation is allowed in the transaction i.e withdraw, refund or distribute. It is part or *oracle* functionality. | https://gitlab.com/nunet/tokenomics-api/wallet-server |
| elasticsearch | NuNet infrastructure | Database to collect platform telemetry information and build the monitoring, analysis and accounting system based on it. | **link?** |
| logbin | NuNet infrastructure | Database to log job data during execution (outdates). | **link?** |
| smart contract | core platform component | Smart contract that is deployed on the blockchain to manage NTX transactions | **link?** |
| Cardano node | NuNet infrastructure | Device that allows to interact with the blockchain. | **link?** |
| Vault | NuNet infrastructure | Secrets management solution for decentralized networks, which acts as secretes management within NuNet infrastructure | **link?** |
