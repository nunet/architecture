## Consolidation of issues

From:
* [List by Janaina](https://gitlab.com/nunet/architecture/-/issues/344#note_1953286846)
* [List by Dagim](https://gitlab.com/nunet/architecture/-/issues/344#note_1958042861)
* [List by Kabir](https://gitlab.com/nunet/architecture/-/issues/344#note_1948465089)

DMS class diagram:
* [in proposed branch](https://gitlab.com/nunet/open-api/platform-data-model/-/blob/proposed/device-management-service/models/classDiagrams/dms-global.mermaid?ref_type=heads)

1. Actor interface;_[@dagimsisay [created an issue](https://gitlab.com/nunet/device-management-service/-/issues/473), discussed with Mobin and to be described in 2024-06-27/28]_
    1. Messaging interface (ability to send and receive messages):
       1. gossip implementation in network package
       2. publish implementation to topic
       3. Remote procedure call logic implementation :interface for unwrapping messages and initiating behaviors based on them;_[@kabir.kbr creates and issue and marks for description / discusssion with Dagim and Mobin]_
       4. logic to send a message to:
         1. directly to another actor (to be used by Nodes and Allocations);
         2. broadcasting to a topic;
    2. Misc requirements / ideas:
       1. can be implemented with a library such as [protoactor-go](https://github.com/asynkron/protoactor-go) however it would need to be certain that we don't import unnecessary complexity (@kabir.kbr -- would be preferrable, but we do not need to import full the package, we can re-use components and ideas, as our licenses are the same; however we need to analyze the package to see if it will do what we want, importing fully would be a good option, but first we need to understand what it does);
       2. if custom implementation (since we have certain requirements in the future, we may need to at least understand how much we can extend the protoactor-go package with them):
          1. id generator the includes a public/private key pair.
          2. a dedicated protocol on the network/libp2p package such as a "/nunet/actor/0.0.1" - this dedicated protocol is needed so that the handler knows to do a forward whenever a message comes on this protocol (@kabir.kbr -- but we would need this anyway, no?) 
          3. [default option, but Dagim will double check with the team] simple message forwarder/router for internal components in the network package that makes it possible to register a handler with an address. This address will be generated when the actor is created however, the network package should at the sametime provide it as a cid on the dht. That way, other Nodes that know the address of the actor will be able to reach it by first looking up for the node providing that id then sending the message on the designated protocol. There're pros and cons to this method that will be discussed in detail on its own issue (for example though, the dedicated protocol at the network/libp2p level will be too specific in the network package and might be complex if promoted up to the actor sub-package). [Dagim creates issues here]
       3. (@kabir.kbr -- we need to make sure that our interfaces define what we want to do, so addresses, pki, etc; it may be good to start with the simple implementation based on our interfaces and then see how can components from protoactor-go can be reused, or maybe even forked and extended);
2. Orchestrator functionalities for job orchestrations (to be used by Actors - Nodes and Allocations):
    1. Capability comparison (related to job formats / machine resource / capability definitions);
       1. Design and describe issues for Capability model in dms package implementation (defined [issue](https://gitlab.com/nunet/open-api/platform-data-model/-/issues/23));
    2. Application level logic for message broadcast (how a node knows a topic / topic definitions);
      1. topics definition (lets define a few topics ourselves now, and then as we go we will define logic for every user / node to start a new topic; first of all define that in the configuration file;)
      2. ~~gossip implementation in network package~~
      3. ~~publish implementation~~
    3. Orchestrator logic (first test: we will start implementing with the job specification that requires one compute provider for one job) (@kabir.kbr -- i would not differentiate this at all: we should consider multiple):
      1. DMS Node (Actor) receives a job definition done by service provider user
      2. DMS Node starts an orchestration process
      3. Orchestrator parses the job definition using the job package
      4. Orchestrator starts the request for bid process using the network package
      5. Orchestrator chooses one DMS-CP to run the workflow using the capability structure comparison
      6. Orchestrator request to DMS-CP to lock the resources (Invocation) - (Tokenomics part will be done in another milestone.)
      7. DMS-CP starts an Allocation
      8. Allocation starts an Executor
      9. Executor runs the job
      10. Allocation sends the logs and result to DMS-SP Node (orchestrator)
      11. Data types and structures as needed:
        1. {list all data types as indicated in the dms class diagram}
      12. Requirements / functionalities;
        1. implement listeners for job request (@kabir.kbr -- why would it be different from general message listeners implemented in network package?) 
      013. Finish job orchestration sequence design in (described [issue](https://gitlab.com/nunet/open-api/platform-data-model/-/issues/24));
        1. list each subsequences that need to be described and sequenced (in issues);
      14. logic to treat a bid request
      15. logic to send a bid answer;
      16. logic to compare bids and choose the best;
      17. implement logic to handle auto-accept (@kabir.kbr -- capabilitly comparison logic; @kabir try to come up with something until 2024-06-24):
         1. if the push started as a pull orchestration and the CP receiving the job spec won the bid
         2. if a whitelisted peer or a peer already in a contract with the CP sent the job spec
         3. if the CP user specified in a config to auto-accept certain types of jobs etc...
         4. main goal is to allow different conditions for automatically accepting an incoming job request
      5. implement logic to handle sending and receiving job invocation request (@kabir.kbr -- sending via general messaging interface to specific machine on orchestrator package) ([issue filed](https://gitlab.com/nunet/device-management-service/-/issues/467) orchestrator package); 
      6. implement a logic to handle events / behaviors after receiving a job invocation ([issue filed](https://gitlab.com/nunet/device-management-service/-/issues/468) jobs package);
      7. requirements from other packages:
         1. implement interfaces for sending a push job request (@kabir.kbr -- via general interface on network);
         2. it should implement functionality that depends on the network interface
         3. use the publish/subscribe functions even if they're not already implemented (@kabir.kbr -- structure to know publish a topic and subscribe to a topic)
      7. logic to handle bid request *[kabir creates and issue and shares on]*;
         1. construct a bid request type on the jobs package (on the node)?
         2. logic for the topics (for the starters we will use single topic);
         3. wrap it into message type so that to be able to send to network for broadcasting;
         4. on the receiver side detect that a message is BidRequest and route it properly;
         5. after that build logic to bid for the bid request (for the starters it could be 'bid for all');
      8. logic to handle bid (@kabir.kbr - if this is a network interface, why would it be different from any other message) *[kabir creates and issue and shares on]*
         1. start with specifying how bid will be handled (@kabir.kbr -- via general message sending / receiving / rpc interface)
            1. by publisher (@kabir.kbr -- constructing bids from job requests?)
            2. by bidder  (@kabir.kbr -- after receiving and unmarshalling);
         2. implement bid logic (might not be any related networking functionality)
            1. compute provider constructs a Bid type, based on the logic of handling of the BidRequest (above);
            2. wraps bid into the message and sends back to the directly to the requester (the address is known);
            3. on the requester side, the message is unwrapped;
            4. the logic of deciding if the bid is accepted;
            1. depend on the interfaces only (not the implementation)
    6. orchestrator graph (node, edge, properties, querying) *(this is still design issue, Kabir wants to think about it;)*
        1. scope and structure (definitions made in the last meeting with Joao);
        2. properties that we will start using
        3. queries that we will start doing
        4. Implement orchestrator.graph subpackage for holding / allowing to reason on network configurations (described [issue](https://gitlab.com/nunet/device-management-service/-/issues/453))      
    7. General requirements:
      1. implementation should be futureproof as possible (no functonal requirements;)
        1. job spec will change in the future (already created)
        2. auto-accept conditions should be extendable with backward compatibility (@kabir.kbr -- why would we need to distinguish auto accept capabilities?); this will be solved on job invocation request functionality;
        3. should depend on general network package messaging interface - not any single implementation etc...
3. Jobs logic for local allocation and executor management (for Node / Allocations to use):
   1. specify parameters needed in the spec and settle on a certain format; [issue](https://gitlab.com/nunet/device-management-service/-/issues/460)
   2. implement parser for that spec to parse job specs (@kabir.kbr -- can we implement generic parser that is able to consider changes in format) [issue](https://gitlab.com/nunet/device-management-service/-/issues/460)
   3. Misc requirements (not only distinct functionality) and relations to other packages: 
    1. the jobs package should be a library provided for the dms/node package
    2. compare spec with capability -- comparison logic is being implemented in orchestrator.matching subpackage; [issue](https://gitlab.com/nunet/device-management-service/-/issues/458);
    3. keep a registry of locally running allocations/executors (jobs package: warrants a separate issue / *[Dagim files an issue]*; 
        1. could be a data structure inside Node actor which we may want to be able to reconstruct / keep persistent in the db (via event))
        2. provides a dms/node/machine view of jobs running on that specific machines.
4. Node functionalities and implementation:
   1. to be created when the dms is onboarded (so as long as dms is running, a node object should be present and running as a main actor residing on the machine) *[Dagim creates issue]* -- initializing the node;  create an issue about creating the concept of node;
   2. its actor-address is peerID in cases where the network package implements the libp2p sub-pkg (@kabir.kbr -- we have actor.id structure, where peerID is one of the addresses; however, are we going to have anything that does not communicate via libp2p?); actors ids as per the model; *[Dagim creates and issue]*
   3. it's supplied with all its dependencies such as database, network and logger/telemetry
   4. it imports and uses job+orchestrator since they don't need any initialisation but instead allow the node to do job and orchestration related work
   5. should check user/free resource before creating allocations to handle work; implement the logic (@kabir.kbr): [in the jobs package:] *[Kabir files an issue + first description; keep the current record]*
        1. the overall machine capacity is estimated during onboarding via dms, which makes the initial available capability;
        2. when each allocation is started, the nodes takes available capability, subtracts allocation capablities and keeps track of current availabilities;
        3. since the allocations will be events, we will be able to reconstruct this from db;
        4. should always keep track of local allocations using functionality from the jobs package
        5. should always keep track of remote allocations using functionality from the orchestrator package (@kabir.kbr -- I would implement this into an allocation)
    2. logic for continuous monitoring *[Dagim files an issue for this]*
5. Allocation functionalities and implementation :
    1. to be created by a Node with the input to new allocation being a description of executors to run along with the amount of resource to allocate; [issue](https://gitlab.com/nunet/device-management-service/-/issues/452)
      1. An allocation is created as a result of 'invocation' message (this is job invocation logic which should be covered with filed issue about that);
      2. (possible) as a result of job posting; *[Kabir proposes the formulation of the bet #2]*
    2. keep track of executors and the amount of resource allocated for those executors (under allocation issue that is defined above;)
    3. handlers to respond to queries about status of executors, amount of resource allocated, used, free etc... (covered above)
    4. Allocation type and methods implementation ([issue](https://gitlab.com/nunet/device-management-service/-/issues/452));
6. General DMS functionality (onboarding, config, etc):
    1. price specification (@kabir.kbr -- I would say this has to be implemented in models, because will be used in dms and jobs specs / packages) *[Kabir files an issue and proposes for Tania]*; in tokenomics package;
    2. improve resource amount probe for all platforms (@kabir.kbr - machine benchmarking functionality) -- it should result in the machine capability spec, so related to models, capability comparison, etc. *[Dagim files an issue]*; this mostly related to GPUs;
    3. deprecate metadata file *done*
    4. deprecate logbin *[Dagim creates an issue -- remove from onboarding function;]*
7. Telemetry
    1. a general logger interface that defines regular logger functionality such as different levels of logging (info, warn, error and debug) including methods to set threshold level for exporter (@kabir.kbr -- mostly implemented, needs to be instrumented and tested);
    2. several implementations of this logger with different collectors:
        1. log to systemd journal *[@Dagim files an issue on this one - assign to Khaled]*
        2. log to db (@kabir.kbr -- already implemented?) [db issue exists]
        3. log to opentelemetry collector server (@kabir.kbr -- already implemented?) [issue exists, but there are comments]
        4. log to elastic search server etc... (@kabir.kbr -- open telemetry collector is not elasticsearch -- should we implement the general elastic collector apart from opentelemetry?)
    3. requirements:
        1. other packages will depend on telemetry in an inverted manner. They will depend on the interface and do their own logging whereas the actual logger implementation will be passed to them through their constructors during initialisation.
    4. Telemetry instrumentation of all packages (for issuing events) and changing all current logging statements to telemetry;
        1. db instrumantation is being done;
        2. storage instrumentatioin is being done;
        3. file issue for each package?_[@janaina.senna creates issue and describes; old packages will be assigned to Khaled and new ones to the team members who are working on their implementation]_ 
8. Executor functionality (mainly done);
9. Supervision functionality (Node / Allocation): (Kabir and Dagim checks on protoacor-go)
    1. health-check / heartbeat (Dagims proposal: differentiate health-check of executors; and heartbeats for allocations;) _[@kabir.kbr files and then we will describe and discuss]_; related issues already filed [1](https://gitlab.com/nunet/device-management-service/-/issues/127); [2](https://gitlab.com/nunet/device-management-service/-/issues/454);
        1. Heartbeat; an allocation pinging a periodic message (backtround task?) to a parent saying i am alive; it will need a process running on a Node on behalf of an Allocation; Implement as a generic Actor functionality; _[Include this into the description of the actor interface (@dagimsisay)]_ _[[issue created](https://gitlab.com/nunet/device-management-service/-/issues/454) and needs to be described]_
        1. Application level healthcheck logic implementation; this will be its own submodule within jobs; whoever executes it becomes an allocation;
        2. definition of healcheck and heartbeat messages (requests / responses / configs)
        3. Design and implement healthcheck / heartbeat interfaces in Allocation (possibly using Actor collector, but not necessarily) _[@kabir.kbr creates these issues]_
        4. related issue [issue](https://gitlab.com/nunet/device-management-service/-/issues/454) _[@kabir.kbr reformulates it];
    2. Implement parent-child relation logic in job deployment process (described [issue](https://gitlab.com/nunet/device-management-service/-/issues/457));
    3. Issues to be implemented (as explained in [blog post](https://nunet.gitlab.io/research/blog/posts/supervisor-model/#proposed-issues-based-on-above-discussion) ):
        1. SupervisorStrategy implementation for Actors; _[@kabir.kbr files and describes]_
        2. Heartbeat implementation for Actors (node and allocation); _[@kabir.kbr files and describes]_
        3. Healthceck implementation for actors (as explained in the blog); _[@kabir.kbr files and describes]_
        4. Healthckeck implementation for allocation->executor relations (as explained in the blog); _[@dagimsisay files and describes]_
10. Tokenomics interface / package:
    1. Design and implement minimal tokenomics package interface (so that we have the notion of contract in each invocation / start of allocation); (@kabir.kbr -- if we are putting orchestrator and jobs packages under dms, maybe we need to put tokenomics under dms too? it will not work without our dms structures, most probably (?))
    2. Review tokenomics design MR and file issues for implementation _[@kabir.kbr files issues based after [MR](https://gitlab.com/nunet/open-api/platform-data-model/-/merge_requests/40#d3eb0ae846293a6f1ce8ac2aad16e2301c3c0754) is reviewed and closed]_
11. Storage:
    1. General design of storage package and finishing it in order to enable storage package to provide general inputs and outputs for a job / allocation; The current one is enough to supply volumes to executors;
    2. Current interface does not deal with a database as storage; Current storage package deals only with the file system;
    3. General storage interface; Possibility is that the current implementation is enough, but @kabir.kbr wants to be sure; the minimal abstract interface is in place, and then we will will be extending in the future;
    4. External database access interface (could be that we will need to develop separate abstract interface for that) _[@kabir.kbr analyses and files an issue / describes]_ (it could be a different package);
12. Test management / environment *[not covered yet]*
    1. unit test coverage should be 80-90% (target for the current dms release (current coverage is 32%)) _[@janaina.senna analyzes and makes issues as needed]_
    1. feature environment running and reporting test results to gitlab and testmo;
    1. integrate dms built into the ci-cd pipeline _[issue to be created by @gabriel.chamon and possibly assigned to Abhishek]_
    1. build all test artifact into html reports (unit tests, runs, behave tests, coverage reports) and upload / publish them on the nunet.io server; _[issue to be created by @gabriel.chamon and possibly assigned to Abhishek]_
    1. Testmo visibility _[issue to be created by @gabriel.chamon and possibly assigned to Abhishek]_
    2. Runnable test scenarios (in Gherkin) to run the scenarios of the feature environment;
        1. functional test for the kubernetes job deployment via interlink plugin; all required infrastructure built;
        2. functional test for the nunet job deployment (of the same kubernetes job description); all required infrastructure and feature environment built;
    3. Pipeline to build and deploy;
    4. Visibility of tests, coverages, stdouts from all machines;
    5. Adapting telemetry framework for accessing test traces from elasticsearch;
    6. goals:
        1. dms pipeline running and reporting unit tests and coverage results to gitlab and testmo;
        2. write unit tests and achieve x% of test coverage (currently )
13. Documentation:
    1. update all readme files on the dms proposed branch as per proposed design (in the platform-data-model repo, proposed branch) *[Pravar is working on that, [issue created]()]*
14. Project management:*[not covered yet]*
    1. Overview and prepare for launch;  
    1. [@janaina will create a new workpackage for Node; @dagim will create a new package on dms];
    1. We have to renew the dms package structure (from the presentation before merge) and communicate / change the dms repository structure; 
15. Network 
    1. private-ip functionality [Hamza is working on [issue](https://gitlab.com/nunet/device-management-service/-/issues/398), scoping it together with @dagimsisay and Sam];
    2. private-swarm functionality *[@dagimsisay reviews joaos implementation and files an issue for finishing it]*
    3. checking local network topology (as was done long time ago in onboarding package, which was checking NAT type, etc) _[@dagimsisay creates and issue and describes]_ (comment -- we may want to keep this information on each dms / node so that to know isp constraints, etc.)


